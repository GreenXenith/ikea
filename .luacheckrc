ignore = {"212", "542"}
read_globals = {
	"minetest",
	"VoxelArea",
	"vector",
	"Raycast",
	"ItemStack",
	"PerlinNoise",
	"ikea",
	"music",
	"util",
	"schematic",
}

read_globals.table = {
	fields = {
		"copy",
		"equals",
		"shuffle",
		"merge",
		"is_in",
		"match",
		"search",
	}
}

read_globals.string = {
	fields = {
		"insert",
		"split",
	}
}

read_globals.vector = {
		fields = {
		"dot",
	}
}

files["mods/util"] = {globals = {"util"}}
files["mods/util/overrides/table.lua"] = {globals = {"table"}}
files["mods/util/overrides/string.lua"] = {globals = {"string"}}
files["mods/util/overrides/vector.lua"] = {globals = {"vector"}}
files["mods/util/schematic.lua"] = {globals = {"schematic"}}

files["mods/ikea/api"] = {globals = {"ikea"}}

files["mods/music"] = {globals = {"music"}}
