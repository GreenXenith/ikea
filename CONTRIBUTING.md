# Rules For Contributing To This Project
By submitting additions or changes of any form to this project, you agree to abide by the following rules:

* All submissions are solely that which you have the adequate legal rights and/or ownership of to abide by the rules of document
* All submissions are to be licensed consistently with the rest of the project unless deemed necessary by a project maintainer
* You must add a `Portions Copyright (C) <year> <name>` line to the LICENSE file, containing your name or a pseudonym, the year or your contribution's creation, and optionaly your email address for ease of contact

# Source Code Formatting Guidelines
All source code submitted to this project is to abide by these guidelines whenever applicable, unless waived by a project maintainer:

_"Bracket" here refers to Curly Brackets, Square Brackets, and Parentheses_

* Use 1 "Hard Tab" to indent each logical section of code
* Encase All "String Literals" in double quotes
* Place a comma after every entree in any multi-line table
* Do not place whitespace:
	* Between a function and it's parameters
	* After any opening bracket
	* Before any closing bracket
* Place Whitespace:
	* Around operators, except in a single-line table or a for-loop constructor
	* Between function arguments
	* Between variables in a multi-line table
* Place any closing bracket used for a registration at the same indentation level as the registration function call
* Place a newline at the end of every source code file

# Texture Creation Guidelines
All textures submitted to this project are to abide by the following guidelines whenever applicable, unless waived by a project maintainer:

* All colors should come from GIMP's "Caramel" palette, if extra colors are needed only the brightness or only the hue of one of these colors should be changed
* All textures should be at the same scale as a standard 16x16 node texture
* Any source files for the textures should be included under a `src` directory inside the `textures` folder inside the mod

# Model Creation Guidelines
All models submitted to this project are to abide by the following guidelines whenever applicable, unless waived by a project maintainer:

* All models should be made to a style consisting of cuboids 1/16th of a meter in size
* Static models (those not animated) should be exported to an OBJ file
* Animated models should be exported to a B3D file
* The source files for the models should be included under a `src` directory inside the `models` folder inside the mod

