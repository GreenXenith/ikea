#!/bin/sh

shopt -s globstar
for filename in **/*.lua; do
	lua-format "$filename" -i "$filename";
done
