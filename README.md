![screenshot](screenshot.png)

**The Infinite IKEA** is a survival horror game for the [Minetest](https://www.minetest.net) game engine based on [SCP-3008](http://www.scp-wiki.net/scp-3008). The goal of this project is to make a fun and interesting game that retains the open-source nature of the SCP Wiki itself.

**This project is not, and has never been associated with Inter IKEA Systems B.V.**

* [Git Repository](https://gitlab.com/benrob0329/ikea)
* [Forum Thread](https://forum.minetest.net/viewtopic.php?f=50&t=22727)
* [Discord Guild](https://discord.gg/YYPF4Ar)
* [Development Video Series](https://www.youtube.com/playlist?list=PL6peF9H3xBeruiqFhIRk58t5cluDsnoaP)

## Contributing

If you would like to contribute to the project, take a look at:

* [CONTRIBUTING.md](CONTRIBUTING.md)
* [Issue Tracker](https://gitlab.com/benrob0329/ikea/issues)
* [Milestones](https://gitlab.com/benrob0329/ikea/-/milestones)

### Licensing
Everything is licensed under the MIT "Expat License" unless otherwise noted.

### Modules
* `ikea`: "Back-End" sort of stuff, things like mapgen and nodes required absolutely everywhere by everything.
	* `api`: Only section of the whole game that touches the `ikea.*` namespace. Defines all global functions and tables inside the game's API.


* `ikea_chairs`: Defines all chair nodes.
* `ikea_warehouse`: Warehouse department. Nodes, schematics, mapgen rules.
* `ikea_showroom`: Showroom department.  Nodes, schematics, mapgen rules.
* `ikea_staff`: Items, Entities, and nodes pertaining to "The Staff".


* `music`: Generic music mod and API with game-specific tracks and implementation.
* `player`: Player API and hand "tool".
* `util`: Generic helper functions.
