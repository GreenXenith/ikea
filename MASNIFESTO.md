# The Infinite IKEA's Game Design Roadmap (AKA The Flatpacked Manifesto)

## Introduction
The purpose of this roadmap is not to define exact mechanics or goals, those can
be found in the milestones and issue tracker; rather it is to define the overall
goals from a design perspective, to define what the game should have for the players
to experience, and what goals they should have throughout the game.

## Premise
The player is trapped in a never ending facility, somewhat resembling that of IKEA brand department of stores.

## Core Gameplay
The game should encourage the following core gameplay elements:

### Settlements
The players should be encouraged to build settlements, and group together to survive.

### Survival
Survival should take two parts, first should be an ever-present, ever-looming threat
that players fight against. It should be the constant factor that players need to
survive against to progress through the game.

The second is the much smaller threat of basic day-to-day life, and the needs that
come from that. This second threat should never overshadow the first, although
neglecting it could result in a severe disadvantage against it.

### Exploration
The players should be rewarded for exploring the area around them, although
exploration should be difficult it ought to get easier as they progress through the game.

## Player Progression
The player should progress through the game by building and upgrading their
settlement, gaining knowledge in how to fight and defend themselves, and discovering
new tools to help with the previous two things.

Difficulty should ramp-up over time, but slowly enough that players can learn to
cope with the increased difficulty.

## End Goals
The end goal of the game is to survive, and eventually escape the facility. Escaping
is not particularly easy though, and may require the use of tools or technologies found along the way.

## Player Death
The player's death should be a potentially disastrous event, with the possibility
of losing everything they have worked for. This should be balanced by making deaths
fairly rare, although not for a lack of difficulty. The player should never feel that
the game was unfair when they die, as the point is to give it weight, not give the
player frustration.
