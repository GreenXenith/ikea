schematic = {}

function schematic.new(size, node)
	node = util.node_or_ignore(node)
	local schem = {size = size, data = {}}

	for _ = 1, (size.x * size.y * size.z) do
		table.insert(schem.data, node)
	end

	return schem
end

function schematic.index(schem, pos)
	return (pos.x) + (pos.y * schem.size.x) + (pos.z * (schem.size.x * schem.size.y)) + 1
end

function schematic.fill_area(schem, node, pos1, pos2)
	node = util.node_or_ignore(node)
	-- Lua does not infer a negative increment
	local x_incr = (pos1.x > pos2.x) and -1 or 1
	local y_incr = (pos1.y > pos2.y) and -1 or 1
	local z_incr = (pos1.z > pos2.z) and -1 or 1

	for z = pos1.z, pos2.z, z_incr do
		for y = pos1.y, pos2.y, y_incr do
			for x = pos1.x, pos2.x, x_incr do
				local index = schematic.index(schem, {x = x, y = y, z = z})
				schem.data[index] = node
			end
		end
	end
end

