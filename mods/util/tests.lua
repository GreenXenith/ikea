local t2s = minetest.serialize
local error_message = [[
Function %s does not pass self-test!
Expected results: %s
Test Results: %s
]]

-- table.equals
local equals_test_table = {"a", "b", c = false, e = {"f", "g", "h", {"h"}}}
table.insert(equals_test_table, equals_test_table)
assert(table.equals(equals_test_table, equals_test_table))
assert(not table.equals({a = 1}, {a = 2}))

-- table.match
local match_test_table = {"foo", "bar", bar = {1, 2, 3}, baz = "boo", foo = {"baz"}}
local match_expected_results = {"foo", bar = {1, 3}, baz = "boo"}
local match_test_results = table.match(match_test_table, {"foo", bar = {1, 3}, baz = "boo", "furb", foo = {"bar"}})
local match_error_message = error_message:format("table.match", t2s(match_expected_results), t2s(match_test_results))
assert(table.equals(match_expected_results, match_test_results), match_error_message)

-- table.search
local search_test_table = {
	{tags = {"a", "b", "c"}, num = 1, num2 = 3},
	{tags = {"b", "c"}, num = 1, num2 = 4},
	{tags = {"b"}, num = 2, num2 = 3},
	{tags = {"b"}, num = 1, num2 = 3},
	{tags = {"a", "b"}, num = 1, num2 = 3},
}

local search_expected_results = {search_test_table[4]}
local search_test_results = table.search(search_test_table,
                            	{includes = {tags = {"b"}, num = 1}, excludes = {tags = {"a"}, num2 = 4}})
local search_error_message =
	error_message:format("table.search", t2s(search_expected_results), t2s(search_test_results))
assert(table.equals(search_test_results, search_expected_results), search_error_message)
