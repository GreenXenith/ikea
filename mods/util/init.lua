util = {}

local modpath = minetest.get_modpath("util")
dofile(modpath .. "/overrides/table.lua")
dofile(modpath .. "/overrides/string.lua")
dofile(modpath .. "/overrides/vector.lua")
dofile(modpath .. "/schematic.lua")

dofile(modpath .. "/tests.lua")

function util.node_or_ignore(node)
	if type(node) == "string" then
		return {name = node}
	elseif type(node) == "table" then
		return node
	else
		return {name = "ignore"}
	end
end

function util.every_n_mapblocks(n)
	return function(value)
		return (value / 16) % n == 0
	end
end

function util.leave_behind(pos, oldnode, oldmetadata, digger)
	if oldmetadata.fields.leave_behind then
		minetest.set_node(pos, {name = oldmetadata.fields.leave_behind})
	end
end

-- Returns The Decimal Places Of A Random Number
function util.tail_perlin(Perlin, x, y, z)
	local num = Perlin:get_3d({x = x, y = y, z = z})
	return num - math.floor(num)
end

-- Returns A Random Number From 1 to limit
function util.bound_perlin(Perlin, limit, x, y, z)
	local num = util.tail_perlin(Perlin, x, y, z)
	return math.floor((num * (limit - 1)) + 0.5) + 1
end
