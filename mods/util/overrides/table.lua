local function half_equals(t1, t2)
	for k, v in pairs(t1) do
		if t2[k] == nil then
			return false
		elseif type(v) == "table" then
			if (v ~= t1) and (not half_equals(t2[k], v)) then
				return false
			end
		elseif not (t2[k] == v) then
			return false
		end
	end
	return true
end

function table.equals(t1, t2)
	return half_equals(t1, t2) and half_equals(t2, t1)
end

table.shuffle = table.shuffle or function(t)
	for i = #t, 2, -1 do
		local j = math.random(i)
		t[i], t[j] = t[j], t[i]
	end
	return t
end

function table.merge(t1, t2)
	for k, v in pairs(t2) do
		t1[k] = v
	end
end

function table.is_in(t, value)
	for _, v in ipairs(t) do
		if (type(value) == "table") and table.equals(t, value) then
			return true
		elseif v == value then
			return true
		end
	end
	return false
end

-- Returns matching values between two tables
function table.match(t1, t2)
	local results = {}
	for k, v in pairs(t2) do
		if (type(k) == "number") and table.is_in(t1, v) then
			table.insert(results, v)
		elseif not t1[k] then
			-- Do Nothing
		elseif type(v) == "table" then
			local tmp_tbl = table.match(t1[k], v)
			results[k] = (not table.equals(tmp_tbl, {})) and tmp_tbl or nil
		elseif t1[k] == t2[k] then
			results[k] = v
		end
	end
	return results
end

-- Returns a sub-set of an indexed table which matches the given parameters
function table.search(t, params)
	params = params or {}
	params.includes = params.includes or {}
	params.excludes = params.excludes or {}
	local results = {}

	for _, entree in ipairs(t) do
		local includes_match = table.match(entree, params.includes)
		local excludes_match = table.match(entree, params.excludes)

		if table.equals(includes_match, params.includes) and table.equals(excludes_match, {}) then
			table.insert(results, entree)
		end
	end

	return results
end
