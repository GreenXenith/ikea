-- Pre-5.1.x compatibility
vector.dot = vector.dot or function(a, b)
	return a.x * b.x + a.y * b.y + a.z * b.z
end
