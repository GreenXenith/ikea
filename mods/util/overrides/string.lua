function string:insert(pos, str)
	return self:sub(1, pos) .. str .. self:sub(pos + 1)
end
