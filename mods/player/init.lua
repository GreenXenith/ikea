local STATIC_GROUPCAP

if minetest.settings:get_bool("creative_mode", false) then
	STATIC_GROUPCAP = {times = {[1] = 1.00}, uses = 0}
end

minetest.register_item(":", {
	type = "none",
	wield_image = "wieldhand.png",
	wield_scale = {x = 1, y = 1, z = 3},
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level = 0,
		groupcaps = {
			static = STATIC_GROUPCAP,

			oddly_breakable_by_hand = {times = {[1] = 1.00}, uses = 0},

			carryable = {times = {[1] = 1.00}, uses = 0},
		},
		damage_groups = {fleshy = 1},
	},
})
