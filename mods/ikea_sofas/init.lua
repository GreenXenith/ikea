local landskrona_nodebox = {
	type = "fixed",
	fixed = {
		{24 / 16, -6 / 16, 6 / 16, 22 / 16, 5 / 16, -8 / 16}, -- Left Armrest
		{-6 / 16, -6 / 16, 6 / 16, -8 / 16, 5 / 16, -8 / 16}, -- Right Armrest
		{22 / 16, -3 / 16, 3 / 16, -6 / 16, 0 / 16, -9 / 16}, -- Seat Cussion
		{22 / 16, 8 / 16, 3 / 16, -6 / 16, 0 / 16, 6 / 16}, -- Back Cussion
		{24 / 16, -6 / 16, -8 / 16, -8 / 16, -3 / 16, -8 / 16}, -- Bottom
		{24 / 16, -6 / 16, 6 / 16, -8 / 16, 5 / 16, 8 / 16}, -- Back
	},
}

local landskrona_blue_name = "Landskrona Sofa (Blue)"
ikea.register_furniture({ -- Furniture Registration
	name = landskrona_blue_name,
	tags = {"sofa", "lounge"},
	box_contents = {"sofas:landskrona"},
	node_name = "sofas:landskrona",
	size_x = 2,
}, { -- Nodedef
	description = landskrona_blue_name,
	mesh = "ikea_sofas_landskrona.obj",
	tiles = {"ikea_sofas_landskrona.png"},
	selection_box = landskrona_nodebox,
	collision_box = landskrona_nodebox,
})
