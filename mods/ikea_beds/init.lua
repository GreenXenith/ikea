local malm_box = {
	type = "fixed",
	fixed = {
		{-0.875, 0.5, 1.4375, 0.875, -0.5, 1.5}, -- Headboard
		{-0.875, 0.0, 1.4375, 0.8125, -0.5, -0.6875}, -- Everything Else
	},
}
local malm_name = "Malm Bed"
ikea.register_furniture({
	name = malm_name,
	tags = {"bed", "bedroom"},
	box_contents = {"beds:malm"},
	node_name = "beds:malm",
	size_x = 2,
	size_z = 2,
}, {
	description = malm_name,
	mesh = "ikea_beds_malm.obj",
	tiles = {"ikea_beds_malm.png"},
	selection_box = malm_box,
	collision_box = malm_box,
})
