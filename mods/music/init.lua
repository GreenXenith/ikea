music = {}

music.tracks = {
	{name = "jahzzar_cddc_aisles", length = 216, title = "Aisles", author = "Jahzzar", license = "CC-BY-SA"},
	{name = "jahzzar_cddc_deadpanned", length = 274, title = "Deadpanned", author = "Jahzzar", license = "CC-BY-SA"},
	{name = "jahzzar_cddc_mantra", length = 176, title = "Mantra", author = "Jahzzar", license = "CC-BY-SA"},
	{name = "jahzzar_cddc_pure", length = 246, title = "Pure", author = "Jahzzar", license = "CC-BY-SA"},
	{name = "jahzzar_cddc_siste_viator", length = 118, title = "Siste Viator", author = "Jahzzar", license = "CC-BY-SA"},
}

music.delay = 5
music.last_played = 0
music.handles = {}

function music.display_song_info(song)
	local message = music.tracks[song].title .. " by " .. music.tracks[song].author .. ", " .. music.tracks[song].license
	minetest.chat_send_all(message)
end

function music.play()
	-- Queue up another play() if it's night
	local is_open = ikea.is_open()
	if not is_open then
		minetest.after(30, music.play)
		return
	end

	-- Randomly choose a different track
	local choose
	repeat
		choose = math.random(1, #music.tracks)
	until choose ~= music.last_played

	music.display_song_info(choose)

	-- Play the song to each player, store the handle in meta
	for _, player in pairs(minetest.get_connected_players()) do
		local handle = minetest.sound_play({
			name = music.tracks[choose].name,
			to_player = player:get_player_name(),
			gain = player:get_meta():get_float("music:gain"),
		})
		music.handles[player:get_player_name()] = handle
	end

	music.last_played = choose
	minetest.after(music.tracks[choose].length + music.delay, music.play)
end

minetest.register_on_newplayer(function(player)
	player:get_meta():set_float("music:gain", 0.1)
	player:get_meta():set_int("music:handle", 0)
end)

minetest.register_chatcommand("volume", {
	description = "Set music volume.",
	params = "<float >= 0>",
	func = function(name, param)
		if tonumber(param) and tonumber(param) >= 0 and minetest.get_player_by_name(name) then
			minetest.get_player_by_name(name):get_meta():set_float("music:gain", tonumber(param))
			return true, "Music volume set to " .. param .. "."
		else
			return false, "Invalid usage, see /help volume."
		end
	end,
})

-- Stop music when it becomes night
local timer = 0
local was_open = false
minetest.register_globalstep(function(dtime)
	timer = timer + dtime
	if timer >= 1 then
		if (not ikea.is_open()) and was_open then
			for _, player in ipairs(minetest.get_connected_players()) do
				local handle = music.handles[player:get_player_name()]
				if handle then
					minetest.sound_stop(handle)
				end
			end
		end
		timer = 0
		was_open = ikea.is_open()
	end
end)

-- Delete Handle When Player Leaves
minetest.register_on_leaveplayer(function(player, timed_out)
	music.handles[player:get_player_name()] = nil
end)

-- Start the music
minetest.after(0, music.play)
