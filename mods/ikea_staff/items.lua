--[[ Staff Items ]] --
-- By GreenXenith
-- These items can be found on staff corpses.
-- CAUTION: May contain bugs.
local LOW_WEAR = 65535

-- Pen. It is used to write on things. Can write 65533 characters in its lifetime.
minetest.register_tool(":staff:pen",
	{description = "Pen", inventory_image = "ikea_staff_item_pen.png", damage_groups = {fleshy = 1}})

-- Functional notepad. Write stuff on it.
local function notepad_show(itemstack, player)
	local meta = itemstack:get_meta()
	local current_page = meta:get_int("current")
	local pages = minetest.deserialize(meta:get_string("pages"))
	if current_page == 0 then
		current_page = 1
	end
	if not pages then
		pages = {}
		for i = 1, 20 do
			pages[i] = ""
		end
	end

	-- Button and pressed button-- Helpers
	local F = minetest.formspec_escape
	local C = minetest.colorize
	local function B(name)
		return "ikea_staff_form_notepad_button.png^ikea_staff_form_notepad_" .. name .. ".png"
	end
	local function P(name)
		return F("(" .. B(name) .. ")^[colorize:#ffffff:100")
	end

	-- Page turn states
	local turn = {prev = {B = B("turn_prev"), P = P("turn_prev")}, next = {B = B("turn_next"), P = P("turn_next")}}
	if current_page == 1 then
		turn.prev.B = turn.prev.B .. F("^[colorize:#000:128")
		turn.prev.P = turn.prev.B
	elseif current_page == #pages then
		turn.next.B = turn.next.B .. F("^[colorize:#000:128")
		turn.next.P = turn.next.B
	end

	local form = ([[
		size[4,8]
		bgcolor[#00000000]
		background[-3,0;10,10;ikea_staff_form_notepad_bg.png]

		image_button[-1.8,4.8;1.2,1.2;%s;save;;true;false;%s]
		image_button[-1.8,6;1.2,1.2;%s;tear;;true;false;%s]
		image_button[-1.8,7.2;1.2,1.2;%s;turn_prev;;true;false;%s]
		image_button[4.6,7.2;1.2,1.2;%s;turn_next;;true;false;%s]
		tooltip[save;Save Text;white;black]
		tooltip[tear;Remove Page;white;darkred]
		tooltip[turn_prev;Previous Page;white;black]
		tooltip[turn_next;Next Page;white;black]

		label[3.5,1.6;%s]
	]]):format(B("save"), P("save"), B("tear"), P("tear"), turn.prev.B, turn.prev.P, turn.next.B, turn.next.P,
             		C("black", current_page .. " / " .. #pages))

	-- Custom line wrap
	local max_len = 30
	local content = string.split(pages[current_page]:gsub("\n\n", "\n \n"):sub(1, 400), "\n")
	for line, text in pairs(content) do
		while text:len() > max_len and text:find(" ") do
			text = text:gsub("%s*$", "")
			local cut = text:match("%S+$")
			text = text:sub(1, -cut:len() - 2)
			content[line] = text
			content[line + 1] = cut .. " " .. (content[line + 1] or "")
		end
	end
	for line, text in pairs(content) do
		form = form .. ("label[0.5," .. 1.7 + (line * 0.4) .. ";%s]"):format(C("black", text))
	end

	local can_write = false
	local inv = player:get_inventory()
	for _, stack in pairs(inv:get_list("main")) do
		if stack:get_name() == "staff:pen" and stack:get_wear() < LOW_WEAR then
			can_write = true
			break
		end
	end

	local written = 2.1 + (#content * 0.4)
	if written < 7.4 and can_write then
		form = form .. "textarea[0.75," .. written .. ";3.5," .. 7.8 - written .. ";content;;]"
	end

	minetest.show_formspec(player:get_player_name(), "notepad", form)
end

minetest.register_craftitem(":staff:notepad", {
	description = "Notepad",
	inventory_image = "ikea_staff_item_notepad.png",
	max_stack = 3,
	on_place = notepad_show,
	on_secondary_use = notepad_show,
})

minetest.register_craftitem(":staff:notepad_used", {
	description = "Used Notepad",
	inventory_image = "ikea_staff_item_notepad_used.png",
	max_stack = 1,
	on_place = notepad_show,
	on_secondary_use = notepad_show,
})

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname:match("^notepad$") then
		local name = player:get_player_name()
		local itemstack = player:get_wielded_item()
		local meta = itemstack:get_meta()
		local current = meta:get_int("current")
		local pages = minetest.deserialize(meta:get_string("pages"))

		if current == 0 then
			current = 1
		end

		if itemstack:get_name() == "staff:notepad" then
			pages = {}
			for i = 1, 20 do
				pages[i] = ""
			end
		end

		if fields.quit then
			return
		end

		if fields.turn_next and current < #pages then
			meta:set_int("current", current + 1)
		elseif fields.turn_prev and current > 1 then
			meta:set_int("current", current - 1)
		end
		if fields.save and fields.content and fields.content ~= "" and fields.content:gsub(" ", "") ~= "" then
			if itemstack:get_name() == "staff:notepad" then
				itemstack = ItemStack("staff:notepad_used")
				itemstack:get_meta():from_table(meta:to_table())
				meta = itemstack:get_meta()
			end
			pages[current] = pages[current] .. "\n" .. fields.content

			local inv = player:get_inventory()
			for i, stack in pairs(inv:get_list("main")) do
				if stack:get_name() == "staff:pen" and stack:get_wear() < LOW_WEAR then
					local wear = stack:get_wear() + fields.content:gsub(" ", ""):len()
					if wear > LOW_WEAR then
						wear = LOW_WEAR
					end
					stack:set_wear(wear)
					inv:set_stack("main", i, stack)
					break
				end
			end

			meta:set_string("pages", minetest.serialize(pages))
		end
		if fields.tear then
			if pages[current]:gsub(" ", "") ~= "" then
				table.remove(pages, current)
				meta:set_string("pages", minetest.serialize(pages))
			end
		end
		player:set_wielded_item(itemstack)
		notepad_show(itemstack, player)
	end
end)

-- Functional stickynote. Write on it. Leave it somewhere. Comes in 4 colors :)
minetest.register_node(":staff:stickynote", {
	description = "Sticky Note",
	drawtype = "signlike",
	tiles = {"ikea_staff_item_stickynote.png"},
	inventory_image = "ikea_staff_item_stickynote.png",
	wield_image = "ikea_staff_item_stickynote.png",
	palette = "ikea_staff_palette_stickynote.png",
	paramtype = "light",
	paramtype2 = "colorwallmounted",
	sunlight_propagates = true,
	walkable = false,
	selection_box = {type = "fixed", fixed = {-0.3, -0.5, -0.3, 0.3, -0.4, 0.3}},
	collision_box = {type = "fixed", fixed = {-0.3, -0.5, -0.3, 0.3, -0.4, 0.3}},
	stack_max = 30,
	groups = {carryable = 1},
	on_rightclick = function(pos, node, clicker, itemstack)
		if itemstack:get_name() == "staff:pen" and itemstack:get_wear() < LOW_WEAR then
			minetest.show_formspec(clicker:get_player_name(), "stickynote" .. minetest.pos_to_string(pos), "field[text;;]")
		end
	end,
})

minetest.register_node(":staff:stickynote_used", {
	description = "Sticky Note with Text",
	drawtype = "signlike",
	tiles = {"ikea_staff_item_stickynote.png"},
	overlay_tiles = {"ikea_staff_item_stickynote_writing.png"},
	inventory_image = "ikea_staff_item_stickynote.png^ikea_staff_item_stickynote_writing.png",
	wield_image = "ikea_staff_item_stickynote.png^ikea_staff_item_stickynote_writing.png",
	palette = "ikea_staff_palette_stickynote.png",
	paramtype = "light",
	paramtype2 = "colorwallmounted",
	sunlight_propagates = true,
	walkable = false,
	selection_box = {type = "fixed", fixed = {-0.3, -0.5, -0.3, 0.3, -0.4, 0.3}},
	collision_box = {type = "fixed", fixed = {-0.3, -0.5, -0.3, 0.3, -0.4, 0.3}},
	stack_max = 30,
	groups = {carryable = 1},
	after_place_node = function(pos, placer, itemstack)
		minetest.get_meta(pos):set_string("infotext", itemstack:get_meta():get_string("message"))
	end,
	preserve_metadata = function(pos, oldnode, oldmeta, drops)
		local meta = drops[1]:get_meta()
		local message = oldmeta.infotext
		meta:set_string("message", message)
		meta:set_string("description", "Sticky Note with Text:\n\"" .. message .. "\"")
	end,
})

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname:match("^stickynote") then
		if fields.text and fields.text ~= "" and fields.text:gsub(" ", "") ~= "" then
			local pos = minetest.string_to_pos(formname:sub(11))
			local node = minetest.get_node(pos)
			minetest.set_node(pos, {name = "staff:stickynote_used", param2 = node.param2})
			local meta = minetest.get_meta(pos)
			local message = fields.text:sub(1, 300)
			meta:set_string("infotext", message)
			local stack = player:get_wielded_item()
			local wear = stack:get_wear() + message:gsub(" ", ""):len()
			if wear > LOW_WEAR then
				wear = LOW_WEAR
			end
			stack:set_wear(wear)
			player:get_inventory():set_stack("main", player:get_wield_index(), stack)
		end
	end
end)

-- ID card. Used to get into staff-only areas, probably.
minetest.register_craftitem(":staff:id", {description = "Employee ID", inventory_image = "ikea_staff_item_id.png"})

-- Scanner. Don't know what it's good for yet.
minetest.register_craftitem(":staff:scanner",
	{description = "PDA Scanner", inventory_image = "ikea_staff_item_scanner.png"})
