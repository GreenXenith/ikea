--[[ Spawning ]] --
-- By GreenXenith
local spawning = {}
spawning.TRIES = 50
spawning.RANGE = 40
spawning.DISTANCE = 10

-- Get players visible from pos (This isnt actually used, yet)
function spawning.visible_players(eye)
	local visible = {}
	for _, obj in pairs(minetest.get_objects_inside_radius(eye, spawning.RANGE)) do
		if obj:is_player() then
			local hidden = true
			local i = 0
			-- Check toes, torso, and head
			while i < 3 and hidden do
				if not Raycast(eye, vector.add(obj:get_pos(), {x = 0, y = i, z = 0}), false):next() then
					hidden = false
				end
				i = i + 1
			end
			if not hidden then
				visible[#visible + 1] = obj:get_player_name()
			end
		end
	end
	return visible
end

-- Check if position is viewable by any player
function spawning.is_visible(pos, height)
	height = height or 1
	height = math.max(1, height)
	for _, player in pairs(minetest.get_connected_players()) do
		local ppos = player:get_pos()
		-- We dont care about players way far away
		if vector.distance(ppos, pos) <= spawning.RANGE then
			local eye = table.copy(ppos)
			eye.y = eye.y + player:get_properties().eye_height
			local look = player:get_look_dir()
			local dir = vector.direction(ppos, {x = pos.x, y = pos.y + height, z = pos.z})
			-- Only worry about players facing us
			if vector.dot(look, dir) > 0 then
				-- Check all visible parts
				for i = 0, height do
					if not Raycast(eye, vector.add(pos, {x = 0, y = i, z = 0}), false):next() then
						return true
					end
				end
			end
		end
	end
	return false
end

-- Spawning globalstep
local rate = 30
local timer = 0
minetest.register_globalstep(function(dtime)
	timer = timer + dtime
	if timer >= rate then
		-- Collect player positions
		local positions = {}
		local function closest(position)
			local p
			local d
			for _, pos in pairs(positions) do
				local dist = vector.distance(pos, position)
				if dist < close.dist then
					p, d = pos, dist
				end
			end
			return p, d
		end

		for _, player in pairs(table.shuffle(minetest.get_connected_players())) do
			local ppos = player:get_pos()
			local _, distance = closest(ppos)
			-- Group players together
			if not distance or distance > spawning.RANGE then
				positions[#positions + 1] = ppos
			end
		end

		for _, position in pairs(positions) do
			local count = math.random(0, 2)
			if count > 0 then
				local checked = {}
				for _ = 1, count do
					local success = false
					local tries = 0
					-- This could probably be an algorithmic thing, but random works fine for now
					while not success and tries < spawning.TRIES do
						local try
						repeat
							try = {
								x = position.x + math.random(-spawning.RANGE, spawning.RANGE),
								y = position.y,
								z = position.z + math.random(-spawning.RANGE, spawning.RANGE),
							}
						until try and vector.distance(try, position) >= spawning.DISTANCE and not checked[minetest.pos_to_string(try)]

						local node = minetest.get_node(try)
						if not minetest.registered_items[node.name].walkable then
							if spawning.is_visible(try, 3) then
								tries = tries + 1
							else
								minetest.add_entity(try, "ikea_staff:member")
								success = true
							end
						end

						checked[minetest.pos_to_string(try)] = 1
					end
				end
			end
		end

		timer = 0
	end
end)
