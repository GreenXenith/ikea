local day_color = "#414141"
local night_color = "#303030"

-- Sets the skybox and global light level for player
local function update_player_time(player)
	local is_open = ikea.is_open()

	if is_open then
		player:override_day_night_ratio(0.7)
		player:set_sky(day_color, "plain", {}, false)
	else
		player:override_day_night_ratio(0)
		player:set_sky(night_color, "plain", {}, false)
	end
end

minetest.register_on_joinplayer(function(player)
	update_player_time(player)
end)

-- Loop through each player every second and update their time
local timer = 0
local was_open = false
minetest.register_globalstep(function(dtime)
	timer = timer + dtime

	if timer >= 1 then
		local is_open = ikea.is_open()

		if is_open ~= was_open then
			for _, player in ipairs(minetest.get_connected_players()) do
				update_player_time(player)

				-- Play a "power down" sound whenever it become night
				if not is_open and was_open then
					minetest.sound_play({name = "ikea_power_down", gain = 0.15, pitch = 1.0})
				end
			end
		end
		timer = 0
		was_open = is_open
	end
end)
