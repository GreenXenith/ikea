local modpath = minetest.get_modpath("ikea")

dofile(modpath .. "/api/api.lua")
dofile(modpath .. "/nodes.lua")
dofile(modpath .. "/mapgen.lua")
dofile(modpath .. "/time.lua")
dofile(modpath .. "/light.lua")
