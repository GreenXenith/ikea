ikea = {}
ikea.mapgen_options = {departments = {}, min_size = 5, max_size = 8}
ikea.registered_furniture = {}

local apipath = minetest.get_modpath("ikea") .. "/api"
dofile(apipath .. "/register.lua")
dofile(apipath .. "/global_state.lua")
