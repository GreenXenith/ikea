local defaults = {
	department = {
		name = "rename me",
		get_schematic = function(edges)
			local schem = {size = {x = 1, y = 1, z = 1}, data = {{name = "ikea:error"}}}
			return schem, nil
		end,
		on_place = function(context, vm, x, z)
		end,
	},

	light = {
		description = "Default Description For Lights",
		paramtype = "light",
		paramtype2 = "facedir",
		drawtype = "mesh",
		mesh = "error.obj",
		tiles = {"unknown_node.png^[colorize:#ff0000:255"},
		use_texture_alpha = true,
		collision_box = nil,
		selection_box = nil,
		light_source = minetest.LIGHT_MAX,
		groups = {static = 1, ikea_light = 1},
	},

	furniture_registration = {
		name = "Default Name For Furniture Registration",
		tags = {"default"},
		box_contents = {},
		node_name = "ikea:error",
		size_x = 1,
		size_y = 1,
		size_z = 1,
	},

	furniture_nodedef = {
		description = "Default Description For Furniture Kits",
		paramtype = "light",
		paramtype2 = "facedir",
		drawtype = "mesh",
		mesh = "error.obj",
		tiles = {"unknown_node.png^[colorize:#ff0000:255"},
		groups = {carryable = 1},
		after_dig_node = util.leave_behind,
	},
}

function ikea.register_department(def_raw)
	def_raw = def_raw or {}

	-- Use new table to avoid changing the defaults table
	local def = {}
	table.merge(def, defaults.department)
	table.merge(def, def_raw)

	table.insert(ikea.mapgen_options.departments, def)
end

function ikea.register_light(name, def_raw)
	def_raw = def_raw or {}

	-- Use new table to avoid changing the defaults table
	local def = {}
	table.merge(def, defaults.light)
	table.merge(def, def_raw)

	-- Register base light node
	minetest.register_node(":" .. name, def)

	-- Register off version of it with no emitted light
	local def_off = def
	def_off.light_source = 0

	-- Make semi-transparent bits invisible
	def_off.use_texture_alpha = false

	minetest.register_node(":" .. name .. "_off", def_off)
end

function ikea.register_furniture(def_raw, nodedef_raw)
	-- Use new table to avoid changing the defaults table
	local def = {}
	table.merge(def, defaults.furniture_registration)
	table.merge(def, def_raw)

	local nodedef = {}
	table.merge(nodedef, defaults.furniture_nodedef)
	table.merge(nodedef, nodedef_raw)

	minetest.register_node(":" .. def.node_name, nodedef)

	-- Register Kit Into Global Table
	table.insert(ikea.registered_furniture, def)
end
