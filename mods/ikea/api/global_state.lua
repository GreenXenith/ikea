function ikea.is_open()
	local tod = minetest.get_timeofday()
	return tod >= (7 / 24) and tod <= (23 / 24)
end
