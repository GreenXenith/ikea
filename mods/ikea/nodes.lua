minetest.register_node("ikea:error", {
	drawtype = "mesh",
	mesh = "error.obj",
	tiles = {"unknown_node.png^[colorize:#ff0000:255"},
	pointable = false,
	walkable = false,
})

minetest.register_node("ikea:invisible_wall", {
	paramtype = "light",
	description = "Invisible Node For Collisions (You Hacker!)",
	drawtype = "airlike",
	walkable = true,
	pointable = false,
	is_ground_content = true,
	sunlight_propagates = true,
})

minetest.register_node("ikea:ceiling", {
	paramtype = "light",
	description = "Ceiling Node (You Hacker!)",
	tiles = {{name = "ikea_ceiling.png", scale = 16, align_style = "world"}},
	groups = {static = 1},
	sunlight_propagates = true,
})

minetest.register_node("ikea:box", {
	description = "Cardboard Box",
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	node_box = {type = "fixed", fixed = {-6 / 16, -0.499, -6 / 16, 6 / 16, 4 / 16, 6 / 16}},
	tiles = {
		{name = "ikea_box_top.png", backface_culling = false},
		{name = "ikea_box_bottom.png", backface_culling = false},
		{name = "ikea_box_side.png", backface_culling = false},
		{name = "ikea_box_side.png", backface_culling = false},
		{name = "ikea_box_front.png", backface_culling = false},
		{name = "ikea_box_front.png", backface_culling = false},
	},
	groups = {carryable = 1},

	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		local node_name = minetest.get_meta(pos):get_string("node_name")
		local contents = table.search(ikea.registered_furniture, {includes = {node_name = node_name}})[1]["box_contents"]
		minetest.swap_node(pos, {name = "ikea:box_open"})

		for _, v in pairs(contents) do
			local vel = math.random(5, 8)
			minetest.spawn_item(pos, v):set_velocity({x = 0, y = vel, z = 0})
		end

		minetest.get_node_timer(pos):start(5)
	end,

	preserve_metadata = function(pos, oldnode, oldmeta, drops)
		drops[1]:get_meta():set_string("node_name", oldmeta.node_name)
	end,

	after_place_node = function(pos, placer, itemstack, pointed_thing)
		minetest.get_meta(pos):set_string("node_name", itemstack:get_meta():get_string("node_name"))
		return false
	end,

	after_dig_node = util.leave_behind,
})

minetest.register_node("ikea:box_open", {
	description = "Cardboard Box, Open (You Hacker!)",
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	node_box = {type = "fixed", fixed = {-6 / 16, -0.499, -6 / 16, 6 / 16, 8 / 16, 6 / 16}},
	tiles = {
		{name = "ikea_box_top.png^[opacity:0", backface_culling = false},
		{name = "ikea_box_bottom.png", backface_culling = false},
		{name = "ikea_box_side.png", backface_culling = false},
		{name = "ikea_box_side.png", backface_culling = false},
		{name = "ikea_box_open_back.png", backface_culling = false},
		{name = "ikea_box_open_front.png", backface_culling = false},
	},
	groups = {oddly_breakable_by_hand = 1},
	drop = "",

	on_timer = function(pos, elapsed)
		minetest.set_node(pos, {name = "air"})
	end,
})
