local vedbo_nodebox = {
	type = "fixed",
	fixed = {
		{-7 / 16, -8 / 16, -7 / 16, 7 / 16, 2 / 16, 7 / 16}, -- Bottom
		{-7 / 16, 2 / 16, 6 / 16, 7 / 16, 16 / 16, 7 / 16}, -- Back
		{-7 / 16, 2 / 16, -7 / 16, -5 / 16, 4 / 16, 6 / 16}, -- Right Armrest
		{5 / 16, 2 / 16, -7 / 16, 7 / 16, 4 / 16, 6 / 16}, -- Left Armrest
	},
}

local vedbo_pink_name = "Vedbo Chair (Pink)"
ikea.register_furniture({ -- Furniture Registration
	name = vedbo_pink_name,
	tags = {"chair", "lounge"},
	box_contents = {"chairs:vedbo"},
	node_name = "chairs:vedbo",
}, { -- Nodedef
	description = vedbo_pink_name,
	mesh = "ikea_chairs_vedbo.obj",
	tiles = {"ikea_chairs_vedbo.png"},
	selection_box = vedbo_nodebox,
	collision_box = vedbo_nodebox,
})

local svenbertil_box = {
	type = "fixed",
	fixed = {
		{-0.437484, 0.937491, 0.313884, 0.437516, 0.124991, 0.376384}, -- Back
		{-0.437484, 0.124991, -0.436116, 0.437516, 0.062491, 0.376384}, -- Seat
		{-0.374984, 0.062491, -0.373616, -0.312484, -0.500009, 0.313884}, -- Left
		{0.312516, 0.062491, -0.373616, 0.375016, -0.500009, 0.313884}, -- Right
	},
}
local svenbertil_name = "Svenbertil Chair"
ikea.register_furniture({ -- Furniture Registration
	name = svenbertil_name,
	tags = {"chair", "kitchen"},
	box_contents = {"chairs:svenbertil"},
	node_name = "chairs:svenbertil",
}, { -- Nodedef
	description = svenbertil_name,
	mesh = "ikea_chairs_svenbertil.obj",
	tiles = {"ikea_chairs_svenbertil.png"},
	selection_box = svenbertil_box,
	collision_box = svenbertil_box,
})
