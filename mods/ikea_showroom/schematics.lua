local schems = {}
schems.floor = schematic.new({x = 16, y = 1, z = 16}, "showroom:floor")
local place_opening = util.every_n_mapblocks(4)
local wall_height = 10

-- Corner North/South East/West Top/Bottom
local corner_swb = {x = 0, y = 0, z = 0}
local corner_seb = {x = 15, y = 0, z = 0}
local corner_nwb = {x = 0, y = 0, z = 15}
local corner_neb = {x = 15, y = 0, z = 15}

local corner_swt = {x = 0, y = wall_height, z = 0}
local corner_set = {x = 15, y = wall_height, z = 0}
local corner_nwt = {x = 0, y = wall_height, z = 15}
local corner_net = {x = 15, y = wall_height, z = 15}

local rooms = {"lounge", "kitchen", "bedroom"}
local display_tables = {
	{
		name = "Table and Chairs",
		nodes = {
			{pos = {x = 1, y = 0, z = 2}, type = "chair"},
			{pos = {x = 2, y = 0, z = 2}, type = "chair"},
			{pos = {x = 1, y = 0, z = 1}, type = "table"},
		},
		rooms = {"lounge", "kitchen"},
	},
	{
		name = "Sofa and Chairs",
		nodes = {{pos = {x = 1, y = 0, z = 2}, type = "sofa"}, {pos = {x = 1, y = 0, z = 1}, type = "table"}},
		rooms = {"lounge"},
	},
	{name = "Bed", nodes = {{pos = {x = 2, y = 0, z = 1}, type = "bed"}}, rooms = {"bedroom"}},
}

function schems.get(Perlin, edges, x, z)
	local schem = schematic.new({x = 16, y = 16, z = 16}, "air")
	-- local has_opening = util.bound_perlin(Perlin, 3, x, 4, z) == 1

	-- Walls
	-- Corners First
	if edges.s and edges.w then
		schematic.fill_area(schem, "showroom:wall", corner_swb, corner_set)
		schematic.fill_area(schem, "showroom:wall", corner_swb, corner_nwt)
	elseif edges.s and edges.e then
		schematic.fill_area(schem, "showroom:wall", corner_seb, corner_swt)
		schematic.fill_area(schem, "showroom:wall", corner_seb, corner_net)
	elseif edges.n and edges.e then
		schematic.fill_area(schem, "showroom:wall", corner_neb, corner_nwt)
		schematic.fill_area(schem, "showroom:wall", corner_neb, corner_set)
	elseif edges.n and edges.w then
		schematic.fill_area(schem, "showroom:wall", corner_nwb, corner_net)
		schematic.fill_area(schem, "showroom:wall", corner_nwb, corner_swt)
	elseif edges.s then
		schematic.fill_area(schem, "showroom:wall", corner_swb, corner_set)
	elseif edges.n then
		schematic.fill_area(schem, "showroom:wall", corner_nwb, corner_net)
	elseif edges.e then
		schematic.fill_area(schem, "showroom:wall", corner_seb, corner_net)
	elseif edges.w then
		schematic.fill_area(schem, "showroom:wall", corner_swb, corner_nwt)
	end

	-- Furniture
	if not (((edges.n or edges.s) and place_opening(x)) or ((edges.e or edges.w) and place_opening(z))) then
		local room_name = rooms[util.bound_perlin(Perlin, #rooms, x, 0, z)]
		local applicable_displays = table.search(display_tables, {includes = {rooms = {room_name}}})

		for x2 = 2, 13, 4 do
			for z2 = 2, 13, 4 do
				local display = applicable_displays[util.bound_perlin(Perlin, #applicable_displays, x2, 1, z2)]

				for _, v in ipairs(display.nodes) do
					local applicable_furniture = table.search(ikea.registered_furniture, {includes = {tags = {v.type, room_name}}})
					local id = util.bound_perlin(Perlin, #applicable_furniture, x + x2, 2, z + z2)

					local index = schematic.index(schem, vector.add(v.pos, {x = x2, y = 1, z = z2}))
					schem.data[index] = {name = applicable_furniture[id].node_name}
				end
			end
		end
	end

	-- Inner Walls
	if (util.bound_perlin(Perlin, 10, x, 5, z) > 3) and not place_opening(x) then
		schematic.fill_area(schem, "showroom:wall", {x = 0, y = 0, z = 0}, {x = 0, y = wall_height, z = 15})
	end
	if (util.bound_perlin(Perlin, 10, x, 6, z) > 7) and not place_opening(z) then
		schematic.fill_area(schem, "showroom:wall", {x = 0, y = 0, z = 0}, {x = 15, y = wall_height, z = 0})
	end

	-- Doorways
	if place_opening(x) then
		if edges.s then
			if edges.inner.s then
				schematic.fill_area(schem, "air", {x = 3, y = 0, z = 0}, {x = 12, y = 4, z = 0})
			else
				schematic.fill_area(schem, "air", {x = 6, y = 0, z = 0}, {x = 9, y = 4, z = 0})
			end
		end
		if edges.n then
			if edges.inner.n then
				schematic.fill_area(schem, "air", {x = 3, y = 0, z = 15}, {x = 12, y = 4, z = 15})
			else
				schematic.fill_area(schem, "air", {x = 6, y = 0, z = 15}, {x = 9, y = 4, z = 15})
			end
		end
	end

	if place_opening(z) then
		if edges.e then
			if edges.inner.e then
				schematic.fill_area(schem, "air", {x = 15, y = 0, z = 1}, {x = 15, y = 4, z = 10})
			else
				schematic.fill_area(schem, "air", {x = 15, y = 0, z = 3}, {x = 15, y = 4, z = 6})
			end
		end
		if edges.w then
			if edges.inner.w then
				schematic.fill_area(schem, "air", {x = 0, y = 0, z = 1}, {x = 0, y = 4, z = 10})
			else
				schematic.fill_area(schem, "air", {x = 0, y = 0, z = 3}, {x = 0, y = 4, z = 6})
			end
		end
	end

	schematic.fill_area(schem, "showroom:floor", {x = 0, y = 0, z = 0}, {x = 15, y = 0, z = 15})
	return schem
end

return schems
