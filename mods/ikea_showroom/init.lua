local modpath = minetest.get_modpath("ikea_showroom")
local schems = dofile(modpath .. "/schematics.lua")

local Perlin = PerlinNoise({
	offset = 0,
	scale = 15,
	spread = {x = 10, y = 10, z = 10},
	seed = 2,
	octaves = 2,
	persistence = 3,
	lacunarity = 2,
	flags = "defaults, absvalue",
})

minetest.register_node(":showroom:floor", {
	paramtype = "light",
	description = "Base Floor Node, Do Not Place (You Hacker!)",
	tiles = {{name = "ikea_showroom_concrete.png"}},
	groups = {static = 1},
	sunlight_propagates = true,
})

minetest.register_node(":showroom:wall", {
	paramtype = "light",
	description = "Base Wall Node, Do Not Place (You Hacker!)",
	tiles = {{name = "ikea_showroom_wall.png"}},
	groups = {static = 1},
	sunlight_propagates = true,
})

ikea.register_department({
	name = "showroom",
	get_schematic = function(edges, x, z)
		return schems.get(Perlin, edges, x, z), nil
	end,
})
