local hemnes_box = {type = "fixed", fixed = {{-0.25, 0.25, -0.1875, 0.25, -0.5, 0.1875}}}
local hemnes_name = "Hemnes Nightstand"
ikea.register_furniture({ -- Furniture Registration
	name = hemnes_name,
	tags = {"nightstand", "bedroom"},
	box_contents = {"nightstands:hemnes"},
	node_name = "nightstands:hemnes",
}, { -- Nodedef
	description = hemnes_name,
	mesh = "ikea_nightstands_hemnes.obj",
	tiles = {"ikea_nightstands_hemnes.png"},
	selection_box = hemnes_box,
	collision_box = hemnes_box,
})
