local schems = {}

-- Node Tables For The schematics
local node_floor = {name = "warehouse:floor"}
local node_rack = {name = "warehouse:rack"}
local node_air = {name = "air"}
local node_rack_light = {name = "warehouse:light", param2 = 1}
local node_rack_ray = {name = "warehouse:light_ray", param2 = 1}
local node_aisle_light = {name = "warehouse:light", param2 = 0}
local node_aisle_ray = {name = "warehouse:light_ray", param2 = 0}
local node_row_sign = {name = "warehouse:row_sign"}

local function place_racks(schem, x, z, num)
	for i = 0, num - 1 do
		local x2 = x + (4 * i)
		local index = schematic.index(schem, {x = x2, y = 0, z = z})
		schem.data[index] = node_rack
	end
end

local function place_row_signs(schem, row_sign_positions)
	for _, v in ipairs(row_sign_positions) do
		local index1 = schematic.index(schems.aisle, {x = 0, y = 7, z = v})
		local index2 = schematic.index(schems.aisle, {x = 15, y = 7, z = v})

		schem.data[index1] = node_row_sign
		schem.data[index2] = node_row_sign
	end
end

-- Just a floor, used for borders currently
schems.floor = schematic.new({x = 16, y = 1, z = 16}, node_floor)

-- Rack Schematic
schems.rack = schematic.new({x = 16, y = 16, z = 16}, node_air)
schematic.fill_area(schems.rack, node_floor, {x = 0, y = 0, z = 0}, {x = 15, y = 0, z = 15})
place_racks(schems.rack, 0, 1, 4)
place_racks(schems.rack, 0, 9, 4)

local rack_light_index_1 = schematic.index(schems.rack, {x = 8, y = 10, z = 3})
local rack_light_index_2 = schematic.index(schems.rack, {x = 8, y = 10, z = 12})
local rack_ray_index_1 = schematic.index(schems.rack, {x = 8, y = 9, z = 3})
local rack_ray_index_2 = schematic.index(schems.rack, {x = 8, y = 9, z = 12})

schems.rack.data[rack_light_index_1] = node_rack_light
schems.rack.data[rack_light_index_2] = node_rack_light

schems.rack.data[rack_ray_index_1] = node_rack_ray
schems.rack.data[rack_ray_index_2] = node_rack_ray

-- Aisle Schematic
schems.aisle = schematic.new({x = 16, y = 16, z = 16}, node_air)
schematic.fill_area(schems.aisle, node_floor, {x = 0, y = 0, z = 0}, {x = 15, y = 0, z = 15})
place_row_signs(schems.aisle, {0, 1, 8, 9})

local aisle_light_index_1 = schematic.index(schems.aisle, {x = 2, y = 10, z = 8})
local aisle_light_index_2 = schematic.index(schems.aisle, {x = 13, y = 10, z = 8})
local aisle_ray_index_1 = schematic.index(schems.aisle, {x = 2, y = 9, z = 8})
local aisle_ray_index_2 = schematic.index(schems.aisle, {x = 13, y = 9, z = 8})

schems.aisle.data[aisle_light_index_1] = node_aisle_light
schems.aisle.data[aisle_light_index_2] = node_aisle_light
schems.aisle.data[aisle_ray_index_1] = node_aisle_ray
schems.aisle.data[aisle_ray_index_2] = node_aisle_ray

-- North Schematic
schems.north = schematic.new({x = 16, y = 16, z = 16}, node_air)
schematic.fill_area(schems.north, node_floor, {x = 0, y = 0, z = 0}, {x = 15, y = 0, z = 15})
place_racks(schems.north, 0, 1, 4)

-- North Aisle Schematic
schems.north_aisle = schematic.new({x = 16, y = 16, z = 16}, node_air)
schematic.fill_area(schems.north_aisle, node_floor, {x = 0, y = 0, z = 0}, {x = 15, y = 0, z = 15})
place_row_signs(schems.north_aisle, {0, 1})

-- South Schematic
schems.south = schematic.new({x = 16, y = 16, z = 16}, node_air)
schematic.fill_area(schems.south, node_floor, {x = 0, y = 0, z = 0}, {x = 15, y = 0, z = 15})
place_racks(schems.south, 0, 9, 4)

-- South Aisle Schematic
schems.south_aisle = schematic.new({x = 16, y = 16, z = 16}, node_air)
schematic.fill_area(schems.south_aisle, node_floor, {x = 0, y = 0, z = 0}, {x = 15, y = 0, z = 15})
place_row_signs(schems.south_aisle, {8, 9})

return schems
