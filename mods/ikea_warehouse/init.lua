local modpath = minetest.get_modpath("ikea_warehouse")
dofile(modpath .. "/nodes.lua")
local schems = dofile(modpath .. "/schematics.lua")

local applicable_furniture
local should_place_aisle = util.every_n_mapblocks(4)
local Perlin = PerlinNoise({
	offset = 0,
	scale = 15,
	spread = {x = 10, y = 10, z = 10},
	seed = 1,
	octaves = 2,
	persistence = 3,
	lacunarity = 2,
	flags = "defaults, absvalue",
})

minetest.register_on_mods_loaded(function()
	applicable_furniture = table.search(ikea.registered_furniture, {includes = {size_x = 1, size_y = 1, size_z = 1}})
end)

-- Places contents for a 16-node long row of racks
local function place_rack_contents(vm, pos, rotate)
	-- Rotate 180 degrees to face the right direction
	local rotation = rotate and 2 or 0

	for x = pos.x, pos.x + 15 do
		local furniture_id = util.bound_perlin(Perlin, #applicable_furniture, x, pos.y, pos.z)
		local furniture_node = {name = applicable_furniture[furniture_id].node_name, param2 = rotation}
		local box_node = {name = "ikea:box"}
		local filler_node = {name = "ikea:invisible_wall"}

		local schem = {
			size = {x = 1, y = 7, z = 1},
			data = {box_node, box_node, furniture_node, filler_node, box_node, box_node, box_node},
		}
		minetest.place_schematic_on_vmanip(vm, {x = x, y = pos.y, z = pos.z}, schem, 0, true, "")

		local box_positions = {
			{x = x, y = pos.y, z = pos.z},
			{x = x, y = pos.y + 1, z = pos.z},
			{x = x, y = pos.y + 4, z = pos.z},
			{x = x, y = pos.y + 5, z = pos.z},
			{x = x, y = pos.y + 6, z = pos.z},
		}

		for j = 1, #box_positions do
			local meta = minetest.get_meta(box_positions[j])
			meta:set_string("node_name", applicable_furniture[furniture_id].node_name)
			meta:set_string("leave_behind", "ikea:invisible_wall")
		end

		local furniture_meta = minetest.get_meta({x = x, y = pos.y + 2, z = pos.z})
		furniture_meta:set_string("leave_behind", "ikea:invisible_wall")
	end
end

ikea.register_department({
	name = "warehouse",

	get_schematic = function(edges, x, z)
		local full_content_positions = {
			{x = 0, y = 1, z = 0, rotate = false},
			{x = 0, y = 1, z = 1, rotate = true},
			{x = 0, y = 1, z = 8, rotate = false},
			{x = 0, y = 1, z = 9, rotate = true},
		}

		local north_content_positions = {{x = 0, y = 1, z = 0, rotate = false}, {x = 0, y = 1, z = 1, rotate = true}}

		local south_content_positions = {{x = 0, y = 1, z = 8, rotate = false}, {x = 0, y = 1, z = 9, rotate = true}}

		if (not edges.n) and (not edges.s) and (not edges.e) and (not edges.w) then
			if should_place_aisle(x) then
				return schems.aisle, {}
			else
				return schems.rack, full_content_positions
			end
		elseif edges.n then
			if should_place_aisle(x) and not (edges.e or edges.w) then
				return schems.north_aisle, {}
			else
				return schems.north, north_content_positions
			end
		elseif edges.s then
			if should_place_aisle(x) and not (edges.e or edges.w) then
				return schems.south_aisle, {}
			else
				return schems.south, south_content_positions
			end
		elseif edges.e then
			return schems.rack, full_content_positions
		elseif edges.w then
			return schems.rack, full_content_positions
		else
			return schems.floor, {}
		end
	end,

	-- Place contents in rack positions where applicable
	on_place = function(context, vm, x, z)
		for _, v in ipairs(context) do
			local pos = {x = x + v.x, y = 0 + v.y, z = z + v.z}
			place_rack_contents(vm, pos, v.rotate)
		end
	end,
})
