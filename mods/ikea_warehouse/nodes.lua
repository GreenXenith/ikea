minetest.register_node(":warehouse:floor", {
	paramtype = "light",
	description = "Base Floor Node, Do Not Place (You Hacker!)",
	tiles = {{name = "ikea_warehouse_concrete.png", scale = 16, align_style = "world"}},
	is_ground_content = true,
	groups = {static = 1},
	sunlight_propagates = true,
})

minetest.register_node(":warehouse:rack", {
	paramtype = "light",
	description = "Racks, or big metal shelves that hold boxes",
	drawtype = "mesh",
	mesh = "ikea_warehouse_rack.obj",
	tiles = {name = "ikea_warehouse_rack.png"},
	is_ground_content = true,
	groups = {static = 1},
	sunlight_propagates = true,
})

ikea.register_light("warehouse:light_ray", {
	description = "Lights That Light The Warehouse",
	mesh = "ikea_warehouse_light_ray.obj",
	tiles = {{name = "ikea_warehouse_light_ray.png", backface_culling = true}},
	light_source = minetest.LIGHT_MAX,
})

minetest.register_node(":warehouse:light", {
	description = "Lights That Light The Warehouse",
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "mesh",
	mesh = "ikea_warehouse_light.obj",
	tiles = {{name = "ikea_warehouse_light.png"}},
})

minetest.register_node(":warehouse:row_sign", {
	paramtype = "light",
	description = "Signs To \"Mark\" Each Row",
	drawtype = "nodebox",
	node_box = {type = "fixed", fixed = {{-4 / 16, -8 / 16, -4 / 16, 4 / 16, 8 / 16, 4 / 16}}},
	tiles = {
		{name = "ikea_warehouse_row_sign_top.png"},
		{name = "ikea_warehouse_row_sign_top.png"},
		{name = "ikea_warehouse_row_sign_side.png", scale = 16, align_style = "world"},
		{name = "ikea_warehouse_row_sign_side.png", scale = 16, align_style = "world"},
		{name = "ikea_warehouse_row_sign_side.png", scale = 16, align_style = "world"},
		{name = "ikea_warehouse_row_sign_side.png", scale = 16, align_style = "world"},
	},
	walkable = false,
	pointable = false,
	is_ground_content = true,
	sunlight_propagates = true,
	groups = {static = 1},
})
